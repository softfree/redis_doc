skiplist(redis.h/t_zset.c)
---------------------------

skiplist 在 redis 中的唯一作用，
作为有序表 zset 的实现，
 ``redis`` 中的 ``skiplist`` 具有如下特点：

1. 允许重复的 ``score`` 值：多个不同的 ``member`` 的 ``score`` 值可以相同。

2. 进行对比操作时，不仅要检查 ``score`` 值，还要检查 ``member`` ：当 ``score`` 值可以重复时，单靠 ``score`` 值无法判断一个元素的身份，所以需要连 ``member`` 域都一并检查才行。

3. 每个节点都带有一个高度为 1 层的后退指针，用于从表尾方向向表头方向迭代：当执行 ``zrevrange`` 或 ``zrevrangebyscore`` 这类以逆序处理有序集的命令时，就会用到这个属性。

数据结构(redis.h)
^^^^^^^^^^^^^^^^^

::

		typedef struct zskiplistNode {
	    //member对象
	    robj *obj;
	    
	    //值
	    double score;
	    
	    //后退指针，用于从表尾向表头方向迭代
	    struct zskiplistNode *backward;
	    
	    //层
	    struct zskiplistLevel {
	        
	        //向前指针
	        struct zskiplistNode *forward;
	        
	        //跨越的节点数
	        unsigned int span;
	    } level[];
	} zskiplistNode;

	typedef struct zskiplist {
	    //头节点、尾节点
	    struct zskiplistNode *header, *tail;
	    
	    //节点数量
	    unsigned long length;
	    
	    //目前节点的最大层数
	    int level;
	} zskiplist;

	跳表各种操作可以参考我原来的一篇博文 `skip list <http://www.cnblogs.com/jacksu-tencent/p/3389626.html>`_ 。但是多了一个span变量，表示指针跨越的节点数。