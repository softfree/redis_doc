整数集(intset.h/intset.c)
--------------------------

intset是集合键的底层实现之一，
保存的元素是有序的。

可作为集合键底层实现，
如果一个集合满足以下两个条件：

1 保存可转化为long long类型的元素

2 元素数量不多

数据结构
^^^^^^^^

::

	typedef struct intset {
	    //保存元素所使用类型的长度
	    uint32_t encoding;
	    //保存元素的个数
	    uint32_t length;
	    //保存元素的数组
	    int8_t contents[];
	} intset;

添加元素
^^^^^^^

.. graphviz:: images/intsetAdd.dot

.. note::
	intset初始化的时候是int16_t类型；只支持升级，不支持降级；查找使用二分查找法。