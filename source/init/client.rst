初始化客户端
-------------

客户端使用了linenoise库，
linenoise比较简单，
不需要任何配置，支持单行、多行模式，history命令查询，自动补全等。
help.h是当前所有的命令文件汇总，
用于tab自动补全功能的源数据。

客户端初始化主要通过一下几步：

1.初始化客户端默认状态

2.查看是否终端输出

3.初始化help

4.根据参数初始化变量

5.判断以那种方式工作


初始化客户端默认状态
^^^^^^^^^^^^^^^^^^^

查看是否终端输出
^^^^^^^^^^^^^^^^^^

!isatty(fileno(stdout)) && (getenv("FAKETTY") == NULL)判断是否终端输出，
实现了如下功能：
::

	$ redis-cli exists akey
	(integer) 0
	$ echo $(redis-cli exists akey)
	0
后面一个命令的输出中 (integer) 去哪里了？

看了看 redis-cli 帮助中有个 --raw 选项，可以控制输出格式：
::
	$ redis-cli --raw exists akey
	0

初始化help
^^^^^^^^^^^

调用cliInitHelp()，
初始化help命令，
包括group、command命令。

根据参数初始化变量
^^^^^^^^^^^^^^^^^

调用parseOptions(argc,argv)，
修改config的默认值。

判断以那种方式工作
^^^^^^^^^^^^^^^

client有九种运行模式：
latency、slave、RDB、Pipe、find big keys、stat、scan、交互、eval模式，
根据不同设置，
初始化不同的运行模式，
默认是交互模式。




